#include <iostream>
#include <Windows.h>
#include <conio.h>

using namespace std;

int main() {

	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);

	char choice; //��������� ������
	bool exit = false;


	do {

		const int arraySize = 7; //������ ��������
		bool matrix[arraySize][arraySize] = { {0}, {0} };
		bool formula[4] = { 0 }; //������� ��������

		cout << "������� ��� ������ (a-j) ��� ������� '0' ��� ������" << endl;
		cin >> choice;
		system("cls");

		if (choice == '0')
			exit = true;
		else
		{
			switch (choice)
			{
			case 'a':
				formula[0] = 0;
				formula[1] = 1;
				formula[2] = 1;
				formula[3] = 0;
				break;
			case 'b':
				formula[0] = 1;
				formula[1] = 0;
				formula[2] = 0;
				formula[3] = 1;
				break;
			case 'c':
				formula[0] = 0;
				formula[1] = 1;
				formula[2] = 0;
				formula[3] = 0;
				break;
			case 'd':
				formula[0] = 0;
				formula[1] = 0;
				formula[2] = 0;
				formula[3] = 1;
				break;
			case 'e':
				formula[0] = 0;
				formula[1] = 1;
				formula[2] = 0;
				formula[3] = 1;
				break;
			case 'f':
				formula[0] = 1;
				formula[1] = 0;
				formula[2] = 1;
				formula[3] = 0;
				break;
			case 'g':
				formula[0] = 1;
				formula[1] = 0;
				formula[2] = 0;
				formula[3] = 0;
				break;
			case 'h':
				formula[0] = 0;
				formula[1] = 0;
				formula[2] = 1;
				formula[3] = 0;
				break;
			case 'i':
				formula[0] = 1;
				formula[1] = 1;
				formula[2] = 0;
				formula[3] = 0;
				break;
			case 'j':
				formula[0] = 1;
				formula[1] = 1;
				formula[2] = 0;
				formula[3] = 0;
				break;
			default:
				break;
			}

			for (short side = 0; side < 4; side++) //���� �� �������� �������� � ��������� �������
			{

				if (formula[side] == 1) //���������� ������� � ����� �������
				{
					short halfOfMatrix = arraySize / 2 + arraySize % 2;
					for (short i = 0; i < arraySize; i++) //����� ������� �� 2 �����
					{
						int f = halfOfMatrix - (i + 1);
						f *= (f < 0 ? -1 : 1);
						for (short j = 0; (j + f) < halfOfMatrix; j++)
							matrix[i][j] = 1;
					}
				}

				//������� �������
				bool buffer[arraySize][arraySize];
				for (short i = 0; i < arraySize; i++)
				{
					for (short j = 0; j < arraySize; j++)
					{
						buffer[arraySize - j - 1][i] = matrix[i][j];
					}
				}

				memcpy(matrix, buffer, sizeof(matrix));
			}

			for (short i = 0; i < arraySize; i++) //����� ��������
			{
				for (short j = 0; j < arraySize; j++)
					cout << ((matrix[i][j] == 0) ? " " : "*") << " ";
				cout << endl;
			}

		}
		cout << endl;
		cout << "������� ����� �������...";
		_getch();
		system("cls");

	} while (!exit);
}