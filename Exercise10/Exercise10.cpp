#include <iostream>
#include <Windows.h>
#include <conio.h>

using namespace std;

int main() {

	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);

	const int boardSize = 8; // ������ ��������� ����� N x N
	short length; // ��������� ������
	bool exit = false;

	while (!exit) {
		while ((cout << "������� ������ �������� ��� ������� '0' ��� ������: ") && !(cin >> length) || (length < 0) || (length > 9)) {
			cout << "������� ���������� �����" << endl;
			cin.clear();
			cin.ignore();
		}

		if (length == 0) {
			exit = true;
			break;
		}
		else
		{
			short sign = 0, // ������ ��������� - ��� *
				size = boardSize;

			for (short i = 0; i < size; i++, sign++) // ������ �������
			{
				for (short i = 0; i < length; i++) // ������ ������
				{
					for (short i = 0; i < size; i++, sign++) // ������ ����
					{
						for (short i = 0; i < length; i++) // ������ ������ *
							cout << ((sign % 2 == 0) ? "* " : "- ");
					}
					cout << endl;
				}
			}

			cout << endl;
			cout << "������� ����� �������...";
			_getch();
			system("cls");
		}
	}
}