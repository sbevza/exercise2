#include <iostream>
#include <Windows.h>
#include <conio.h>

using namespace std;

int main() {

	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);

	long number;
	while ((cout << "������� ����� �����: ") && !(cin >> number))
	{
		cout << "�������� ������ �����!" << endl;
		cin.clear();
		cin.ignore();
	}
	system("cls");

	char choice; // ��������� ������
	bool exit = false;
	long buffer = number;

	while (!exit) {
		do
		{
			cout << "�������� ��������:" << endl;
			cout << "a. ����� ���� �����;" << endl;
			cout << "b. ���������� ���� �����;" << endl;
			cout << "c. ������� ��������������;" << endl;
			cout << "d. ���������� 0 � �����;" << endl;
			cout << "f. �����;" << endl;
			cin >> choice;
			system("cls");
		} while (!(choice == 'a' || choice == 'b' || choice == 'c' || choice == 'd' || choice == 'f'));

		number = buffer;

		int quantity = 0; //���������� ����
		int amount = 0; // ����� ����
		int average = 0; // ������� ��������������
		short digit = 0; // �����
		int quantityOf0 = 0; // ���������� 0


		for (; number != 0; quantity++)
		{
			digit = number % 10;
			amount += digit;
			number /= 10;

			if (digit == 0)
				quantityOf0++;
		}

		switch (choice)
		{
		case 'a':
			cout << "����� ���� ����� = " << amount << endl;
			break;
		case 'b':
			cout << "���������� ���� ����� = " << quantity << endl;
			break;
		case 'c':
			average = amount / quantity;
			cout << "������� �������������� = " << average << endl;
			break;
		case 'd':
			cout << "���������� 0 � ����� = " << quantityOf0 << endl;
			break;
		default:
			exit = true;
		}

		cout << "������� ����� �������...";
		_getch();
		system("cls");
	}
}